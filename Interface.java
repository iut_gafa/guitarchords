import java.awt.*;
import javax.swing.*;

public class Interface extends JPanel {
	final static long serialVersionUID = 0;
	private Menu menu;
	private Manche manche;

	public Interface() {
		super();

		//Configuration
		//setPreferredSize(new Dimension(300, 400));
		manche = new Manche(6, this);
		String[] liste_nom_accords = Accord.listAccord();

		menu = new Menu(liste_nom_accords, Accord.accordByResearch(liste_nom_accords[0]).getNotes(), this);

		manche.setFingersPosition(Accord.accordByResearch(liste_nom_accords[0]).getFingerPlace());


		//Insertion dans le menu
		add(menu);
		add(manche);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
	}

	public Manche getManche()
	{
		return manche;
	}

	public Menu getMenu()
	{
		return menu;
	}
}
