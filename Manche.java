import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class Manche extends JPanel implements MouseListener {
	Corde strings[] = new Corde[6];
	int nbCorde;
	final static long serialVersionUID = 0;
	private Interface inter;

	public Manche(int _nbCorde, Interface main_interface) {
		super();
		this.inter = main_interface;

		nbCorde = _nbCorde;

		setPreferredSize(new Dimension(300, 350));
		setLayout(new FlowLayout());
		for (int i = 0; i < nbCorde; ++i) {
			strings[i] = new Corde(i-1);
			strings[i].setPreferredSize(new Dimension(40, 350));
			add(strings[i]);
		}

		addMouseListener(this);
	}

	public void paintComponent(Graphics g) {
		setBackground(new Color(255, 186, 115));
		g.setColor(new Color(255, 186, 115));
		g.fillRect(0, 0, getWidth(), getHeight());
		paintFret(g);
	}

	public void paintFret(Graphics g) {
		//Variables initialisation
		Rectangle size = getBounds();
		Graphics2D g2 = (Graphics2D) g;

		g2.setStroke(new BasicStroke(4));
		g2.setColor(Color.black);
		for (int i = 0; i < 6; ++i) {
			g.drawLine(size.width/14, (i+1)*size.height/7, 13*size.width/14, (i+1)*size.height/7);
		}
	}

	public void setFingersPosition(int[] positions)
	{
		for (Integer i = 0; i < positions.length && i < nbCorde; ++i)
		{
			strings[i].setFretNumber(positions[i]);
		}
		repaint();
	}

	//Implements event managing functions
	public void mousePressed(MouseEvent e) {}
	public void mouseReleased(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}

	public void mouseClicked(MouseEvent e) {
		//Modifie fret position
		int rangCorde = (int)(e.getX()/(getWidth()/nbCorde));
		int hauteur = (int)(e.getY()/(getHeight()/7));
		if (e.getButton() == MouseEvent.BUTTON1)
		{
			strings[rangCorde].setFretNumber(hauteur);
			Integer num_corde = ((nbCorde - rangCorde) -1) % 5;
			Integer position_corde = (((8 * ((num_corde > 1)?1:0)) + ((num_corde - ((num_corde > 1)?1:0)) * 7)) + hauteur) % 12;
			inter.getMenu().setOneNote(rangCorde, Corde.notes_corde[position_corde]);
		}
		else
		{
			strings[rangCorde].setFretNumber(-1);
			inter.getMenu().setOneNote(rangCorde, "(x)");
		}
		// Est ce que on connait cet accords ?

		java.util.List<Integer> fingerPos = new ArrayList<Integer>();
		for (Integer i = 0; i < nbCorde; ++i)
		{
			fingerPos.add(strings[i].getFretNumber());
		}

		inter.getMenu().selectItem(Accord.getAccordName(fingerPos.toArray(new Integer[0])));

		repaint();
	}
}
