import javax.swing.*;
import java.awt.*;

public class Corde extends JPanel {
	int fretNumber;
	final static long serialVersionUID = 0;
	public static String[] notes_corde = new String[] {"E", "F", "F#", "G", "G#", "A", "A#", "B", "C", "C#", "D", "D#"};
	public static Integer nb_notes_corde = notes_corde.length;

	public Corde(int _fretNumber) {
		super();
		fretNumber = _fretNumber;
	}

	public void paintComponent(Graphics g) {
		setBackground(new Color(255, 186, 115));

		paintString(g);
		paintFingerPlace(g);
	}

	public void paintString(Graphics g) {
		//Variables initialisation
		Rectangle size = getBounds();
		Graphics2D g2 = (Graphics2D) g;

		//Drawing
		g2.setColor(Color.black);
		g2.setStroke(new BasicStroke(4));
		g.drawLine(size.width/2, size.height/7, size.width/2, size.height);
	}

	public void paintFingerPlace(Graphics g) {
		//Variables initialisation
		Rectangle size = getBounds();
		Graphics2D g2 = (Graphics2D) g;

		//Select the color
		g2.setColor(new Color(0,0,0));
		//If there is a negative fretNumber we are drawing a cross.
		if (fretNumber < 0) {
			g2.setStroke(new BasicStroke(4));
			g2.drawLine(0, 0, size.width, size.height/8);
			g2.drawLine(0, size.height/8, size.width, 0);
		} else {
			//Else we are drawing a circle at the fretNumber place.
			g.fillOval(0, ((size.height/7)*fretNumber), size.width, size.height/8-5);
		}
	}

//-----------------------------getteurs / setteurs------------------------------
	public int getFretNumber() {
		return fretNumber;
	}

	public void setFretNumber(int fretNumber) {
		this.fretNumber = fretNumber;
	}
}
