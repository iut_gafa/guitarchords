import java.io.*;
import java.util.*;

public class Accord {
	/*private final static String[][] = new String[6][8] {

	};*/
	private final static int nbNote = 6;
	private String name;
	private int fingerPlace[] = new int[nbNote];
	private String notes[] = new String[nbNote];
	private static String chords_path = "./chords";

	//Lit tout le répertoire avec les accords et renvoie la liste :
	public static String[] listAccord()
	{

		File[] chords = new File(chords_path).listFiles();
		if (chords == null)
		{
			System.out.println("Erreur dans -> Accord::listAccord : Le dossier \"" + chords_path + "\" est introuvable.");
			return new String[] { "Null" };
		}


		List<String> retour = new ArrayList<String>();

		for (Integer i = 0; i < chords.length; ++i)
		{
			if (chords[i].isFile())
			{
				retour.add(chords[i].getName().replaceAll(".chord", ""));
			}
		}

		// On la range par ordre croissant :
		Collections.sort(retour);

		if (retour.get(0).equals("???")) // On met ??? à la fin
		{
			retour.remove(0);
			retour.add("???");
		}

		return retour.toArray(new String [0]); // le "new String [0]" est important sinon roArray renvoi un Object[] et non un String[] !

	}

	public static Accord accordByResearch(String nom/*Chemin d'accès fers le fichier ou seulement nom*/)
	{
		File file = new File(chords_path + "/" + nom + ".chord");

		try (BufferedReader br = new BufferedReader(new FileReader(file)))
		{
			String line;
			String line_notes = ((line = br.readLine()) != null)?line:"?:?:?:?:?:?";
			String line_fingers = ((line = br.readLine()) != null)?line:"0:0:0:0:0:0";

			int[] fingers = new int[nbNote];
			String[] fingers_s = line_fingers.split(":", -1);
			for (Integer i = 0; i < nbNote && i < fingers_s.length; ++i)
			{
				fingers[i] = Integer.parseInt(fingers_s[i]);
			}

			return new Accord(nom, fingers, line_notes.split(":", -1));
		}
		catch (Exception e)
		{
			System.out.println("Erreur dans -> Accord::accordByResearch : " + e.getMessage());
			return new Accord();
		}
	}

	//Crée un accord à l'aide de la place des doigts :
	public static Accord accordByComposition(int fingerPlace[])
	{
		return new Accord();
	}

	public static String getAccordName(Integer[] fingerPlace)
	{
		File[] files_and_directories = new File(chords_path).listFiles();
		String nom_accord = "???";
		if (files_and_directories == null)
		{
			System.out.println("Erreur dans -> Accord::listAccord : Le dossier \"" + chords_path + "\" est introuvable.");
			return nom_accord;
		}

		List<File> only_files = new ArrayList<File>();

		for (Integer i = 0; i < files_and_directories.length; ++i)
		{
			if (files_and_directories[i].isFile())
			{
				only_files.add(files_and_directories[i]);
			}
		}

		for (Integer i = 0; i < only_files.size(); ++i)
		{
			try (BufferedReader br = new BufferedReader(new FileReader(only_files.get(i))))
			{
				nom_accord = only_files.get(i).getName().replaceAll(".chord", "");
				String line;
				boolean bool = true;
				line = br.readLine(); // 1 ere ligne
				String line_fingers = ((line = br.readLine()) != null)?line:"0:0:0:0:0:0"; // 2eme

				int[] current_fingers = new int[nbNote];
				String[] fingers_s = line_fingers.split(":", -1);
				for (Integer j = 0; j < nbNote && j < fingers_s.length; ++j)
				{
					current_fingers[j] = Integer.parseInt(fingers_s[j]);
				}

				// Correspond ?
				for (Integer j = 0; j < current_fingers.length && j < fingerPlace.length && bool; ++j)
				{
					bool = (current_fingers[j] == fingerPlace[j]);
				}

				// Si oui on quitte et on renvoie le nom du fichier / Accord
				if (bool)
					break;
				// Sinon on met le nom à "???" :

				nom_accord = "???";
			}
			catch (Exception e)
			{
				System.out.println("Erreur dans -> Accord::getAccordName : " + e.getMessage());
			}
		}


		return nom_accord;

	}

	private Accord(String name, int[] fingerPlace, String[] notes)
	{
		this.name = name;
		this.fingerPlace = fingerPlace;
		this.notes = notes;
	}

	public Accord() {
		for (int i = 0; i < nbNote; ++i) {
			notes[i] = "x";
			fingerPlace[i] = -1;
		}
		name = "NULL";
	}

	/**
	 * @return the fingerPlace
	 */
	public int[] getFingerPlace() {
		return fingerPlace;
	}

	public String[] getNotes()
	{
		return notes;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public void setFingerPlace(int string, int fret) {
		if (name != "NULL") {
			name = "NULL";
			for (Integer i = 0; i < notes.length; ++i) {
				notes[i] = "x";
			}
		}
		fingerPlace[string] = fret;
	}

	public void setFingerPlace(int frets[]) {
		for (int i = 0; i < notes.length; ++i) {
			setFingerPlace(i, frets[i]);
		}
	}
}
