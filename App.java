import java.awt.*;
import javax.swing.*;

public class App extends JFrame {
	final static long serialVersionUID = 0;
	private Interface inter;

	public App(String title) {
		super(title);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		inter = new Interface();
		setContentPane(inter);

		pack();
		setResizable(false);
		setVisible(true);
	}
}
