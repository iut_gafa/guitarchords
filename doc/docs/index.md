# Bienvenue
Bienvenue sur la documentation de ChordsViewers v.1

## Présentation

Le but de ce projet est de proposer une aide aux guitaristes débutants en leur montrant où placer les doigts pour chacun des accords de la liste.
De plus les utilisateurs peuvent placer visuellement l'accord sur la grille et découvrir quel est son nom s'il est connu.

## Utilisation

L'utilisateur à deux actions à disposition :

* Il peut cliquer sur le menu déroulant en haut et choisir l'accord à afficher.
* Et il peut modifier n'importe quelle note de l'accord en cliquant sur la case de la corde correspondante.

**Information** : Si le logiciel connaît l'accord modifié, il affiche le nom de l'accord dans le menu déroulant.

## Installation

Pour lancer le programme :

```
$ java -jar chordsview.jar
```

## Fonctionnement général

Le programme est divisé en classe qui on chacune leur rôle :

- Les [**cordes**](Class/Corde.md) dessine une ligne et un point.
- Le [**manche**](Class/Manche.md) dessine les frets et gère les cordes.
- Le [**menu**](Class/Menu.md) affiche la partie supérieur de l'application (sélection des accords et affichage des notes).
- L'[**interface**](Class/Interface.md) affiche le tout
- [**App**](Class/App.md) paramètre la fenêtre et affiche l'interface
- Finalement la classe [**Accord**](Class/Accord.md) est chargé de savoir lire et retrouver les accord dans le fichier texte.

## Diagramme de classe
![Diagramme de classe](img/classDiagram.jpg)
