# Accord

## Rôle

>Le rôle de la classe accord, permet de manipuler des accords et d'en rechercher selon les notes ou selon la position des doigts sur le manche.

## Fonctionnalités

```java
//------------------------Méthodes de Classes--------------------------------
// Lit tout le répertoire avec les accords et renvoie la liste :
public static String[] listAccord();
// Retourne un accord en fonction de son nom donné en paramètre :
public static Accord accordByResearch(String nom);
// Retourne un accord en fonction des positions des doigts sur le manche :
public static Accord accordByComposition(int fingerPlace[]);
// Retourne le nom d'un accord en fonction des positions des doigts sur le manche :
public static String getAccordName(Integer[] fingerPlace);

//------------------------Constructeur/Destructeur--------------------------------
public Accord();

//----------------------------------Accesseurs----------------------------------
// Retourne un tableau représentant la positions des doigts sur le manche pour réaliser l'accord :
public int[] getFingerPlace();
// Retourne un tableau des noms des notes de l'accord :
public String[] getNotes();
// Retourne le nom de l'accord :
public String getName()

//---------------------------------Modificateurs---------------------------------
// Permet de définir la position d'un doigt sur le manche
public void setFingerPlace(int string, int fret);
// Permet de définir toutes les positions des doigts de l'accord
public void setFingerPlace(int frets[])
```