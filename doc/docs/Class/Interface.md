# Interface

## Rôle

>Le rôle de la classe interface est d'afficher et de gérer le manche et le menu. Ainsi elle permet de faire le lien entre les évènements du manche et ceux du menu.

## Fonctionnalités

```java
//------------------------Constructeur/Destructeur--------------------------------
// Instancie une interface :
public Interface();

//----------------------------------Accesseurs----------------------------------
// Retourne le manche de l'interface :
public Manche getManche();
// Retourne le menu de l'interface :
public Menu getMenu();
```
