# Manche

## Rôle
> La classe manche est chargé d'afficher l'entièreté du manche soit dans notre cas 6 cordes avec les frets.

## Fonctionnalités
```Java
//-------------------------Constructeur / Destructeur-------------------------//
public Manche(int _nbCorde, Interface main_interface)

//---------------------------------Méthodes-----------------------------------//
//Méthode par défaut appelé pour dessiner dans un JPanel.
public void paintComponent(Graphics g)
//Dessine les frets.
public void paintFret(Graphics g)

//Cette fonction prend en paramètre un tableau de n entier où n est le nombre de corde.
//Elle permet donc de paramétré les notes des n cordes d'un coup.
public void setFingersPosition(int[] positions)

//Méthode appelé au moment d'un click de souris, elle est implémenté par l'interface JMouseEvent.
//Ici elle permet de changé la position de la note sur la bonne corde et à la bonne fret.
public void mouseClicked(MouseEvent e)
```
