# Corde

## Rôle
> La classe Corde est chargé de savoir afficher une corde avec le point indiquant la position de la note.

## Fonctionnalités
```Java
//------------------------Constructeur / Destructeur--------------------------//
public Corde(int _fretNumber)

//---------------------------------Méthode------------------------------------//
//Méthode appelé par défaut pour dessiner dans un JPanel.
public void paintComponent(Graphics g)
//Dessine la corde.
public void paintString(Graphics g)
//Dessine le point ou la croix sur ou au dessus de la corde.
public void paintFingerPlace(Graphics g)

//---------------------------------Accesseurs---------------------------------//
//Renvoie la valeur de fretNumber
//Un nombre > 0 représente la position, 0 une corde à vide, -1 une corde non jouée.
public int getFretNumber()
```
