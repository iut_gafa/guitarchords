# Menu

## Rôle

>Le rôle de la classe menu est d'afficher un menu déroulant à l'utilisateur et d'afficher la liste des notes que joue chaque corde. Le menu déroulant à deux fonctions, la première est d'afficher la listes des accords affichables, et d'afficher l'accord si l'utilisateur clique sur celui-ci. La deuxième fonction est d'afficher le nom de l'accord que l'utilisateur est en train de faire.

## Fonctionnalités

```java
//------------------------Constructeur/Destructeur--------------------------------
// Instancie un menu :
public Menu (String[] _accord, String[] _accordage, Interface main_interface);

//---------------------------------Modificateurs----------------------------------
// Permet de définir l'affichage de la note d'une corde :
public void setOneNote(Integer num_note, String note);
// Permet de définir l'affichage de la note de chaque corde :
public void setAllNotes(String[] notes);

//---------------------------------Méthodes----------------------------------
// C'est l'action qui est effectué lorsque l'utilisateur appuie sur un des éléments du menu :
public void actionPerformed(ActionEvent a);
// Sélectionne l'élement à afficher sur le menu :
public void selectItem(String item);
```
