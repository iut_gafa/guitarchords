## Choix Techniques

L'usage de classe bien distincte avec des rôles précis nous a permit de rendre notre code presque entièrement modulable ou du moins nécessitant que peu d'ajustement pour répondre à des besoins futurs.
De plus cela permet de construire le programme pas à pas et rend donc la programmation de plus en plus simple au fur et a mesure que nous avançons dans le projet.

## Algorithmes Complexes

### Calcul d'une note à partir du numéro de la corde et de sa position sur la corde :

Le principe et de trouver à qu'elle case, du tableau suivant, accéder.
```java
public static String[] notes_corde = new String[] {"E", "F", "F#", "G", "G#", "A", "A#", "B", "C", "C#", "D", "D#"};
```
Et l'index est calculé grâce à la ligne suivante :
```java
Integer position_corde = (((8 * ((num_corde > 1)?1:0)) + ((num_corde - ((num_corde > 1)?1:0)) * 7)) + hauteur) % 12;
```

## Format de Fichier

Nous avons créés un format de fichier `.chord`.

Sont fonctionnement est simple, il contient deux lignes :

* La première ligne, qui représente le nom des notes de l'accord, séparés par des "`:`".
* La deuxième ligne, qui représente la position des doigts sur le manche pour faire l'accord, séparés par des "`:`".
