```mermaid
classDiagram
    class Accord {
			- Int NBNOTE = 6
			- String name
			- Int fingerPlace
			- String notes
			- String ~chords_path = "../chords"
			
			- Accord(String name, Int[] fingerPlace, String[] notes)
			+ Accord()
			
			+ String[] ~listAccord()
			+ Accord ~accordByResearch(String nom)
			+ Accord ~accordByComposition(Int[] fingerPlace)
			+ String ~getAccordName(Integer[] fingerPlace)
			
			+ Int[] getFingerPlace()
			+ String[] getNotes()
			+ String getName()
			+ void setFingerPlace(Int string, Int fret)
			+ void setFingerPlace(Int[] frets)
		}
	class App {
			+ App(String title)
	}
	class Corde {
			- Int fretNumber
			+ String[] ~notes_corde = ["E", "F", "F#", "G", "G#", "A", "A#", "B", "C", "C#", "D", "D#]
			+ Integer ~nb_notes_corde
			
			+ Corde(int _fretNumber)
			+ void paintComponent(Graphics g)
			+ void paintString(Graphics g)
			+ void paintFingerPlace(Graphics g)
			
			+ Int getFretNumber()
			+ void setFretNumber(Int fretNumber)
	}
	class Interface {
			+ Interface()
			+ Manche getManche()
			+ Menu getMenu()
	}
	class Manche {
			- Int nbCorde
			
			+ Manche(Int _nbCorde, Interface main_interface)
			+ void paintComponent(Graphics g)
			+ void paintFret(Graphics g)
			+ void setFingersPosition(Int[] positions)
			+ void mouseClicked(MouseEvent e)
	}
	class Menu {
			- JLabel[] accordage
			
			- Menu (String[] _accord, String[] _accordage, Interface main_interface)
			- void setDesign()
			- void setOneNote(Integer num_note, String note)
			- void setAllNotes(String[] notes)
			- void actionPerformed(ActionEvent a)
			- void selectItem(String item)
	}
	
	App --|> JFrame
	App "1"--*"1" Interface
	
	Corde --|> JPanel
	
	Interface --|> JPanel
	Interface "1"--*"1" Menu
	Interface "1"--*"1" Manche
	
	Manche --|> JPanel
	Manche --|> MouseListener : implements
	Manche "1"--*"6" Corde
	Manche "1"--*"1" Interface
	
	Menu --|> JPanel
	Menu --|> ActionListener : implements
	Menu "1"--*"*" Accord
	Menu "1"--*"1" Interface
	
	
	
	
```
