# Conclusion

## Bilan

Ce fûsse un projet moult intéressant ! À l'année prochaine parce qu'on a toujours pas de stage.

## Optimisations Possibles

* Que l'utilisateur ait la possibilité d'ajouter des accords dans la base de données du logiciel alors que le logiciel tourne.

* Utilisation d'un format de fichier plus standard comme le json.

* Gérer la dualité bémol / dièse pour les notes ambiguës.

## Extensions Possibles

* Jouer les sons pour chaque accord.
* Un bouton pour sauvegarder de nouveaux accords.
* Une fenêtre redimensionnable.
