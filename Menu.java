import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Menu extends JPanel implements ActionListener {
	final static long serialVersionUID = 0;
	private JComboBox<String> accord;
	private JLabel[] accordage;
	private Interface inter;

	public Menu (String[] _accord, String[] _accordage, Interface main_interface) {
		super();
		this.inter = main_interface;
		accord = new JComboBox<String>(_accord);
		accordage = new JLabel[inter.getManche().nbCorde];


		accord.addActionListener(this);
		add(Box.createRigidArea(new Dimension(0, 10)));
		add(accord);


		JPanel aff_notes = new JPanel();
		aff_notes.setLayout(new GridBagLayout()); // BoxLayout(aff_notes, BoxLayout.LINE_AXIS)
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(10, 16, 10, 16);
		aff_notes.setBackground(Color.black);
		for (Integer i = 0 ; i < _accordage.length && i < accordage.length; ++i)
		{
			accordage[i] = new JLabel(_accordage[i]);
			aff_notes.add(accordage[i], c);
			aff_notes.add(Box.createHorizontalGlue());
			aff_notes.add(Box.createRigidArea(new Dimension(0, 20)));
		}
		add(aff_notes);

		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		setDesign();
	}

	private void setDesign() {
		setBackground(Color.black);
		for (Integer i = 0 ; i < accordage.length; ++i)
		{
			accordage[i].setForeground(Color.white);
		}

	}

	public void setOneNote(Integer num_note, String note)
	{
		if (num_note < accordage.length)
		{
			accordage[num_note].setText(note);
		}
	}

	public void setAllNotes(String[] notes)
	{
		for (Integer i = 0 ; i < accordage.length; ++i)
		{
			accordage[i].setText(notes[i]);
		}
	}

	public void actionPerformed(ActionEvent a)
	{
		if (a.getSource() == accord)
		{
			Accord new_accord = Accord.accordByResearch((String)(((JComboBox)a.getSource()).getSelectedItem()));
			setAllNotes(new_accord.getNotes());
			inter.getManche().setFingersPosition(new_accord.getFingerPlace());
		}
	}

	public void selectItem(String item)
	{
		accord.removeActionListener(this);
		accord.setSelectedItem(item);
		accord.addActionListener(this);
	}


}
